To build the book template, you'll need the packages included in structural.latex as well as biber. Once you've installed the requisite packages, run:

```bash
pdflatex book-template.latex
biber book-template
pdflatex book-template.latex
```
